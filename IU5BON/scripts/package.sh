#!/bin/bash

set -e

if [ "$CI" == "true" ]; then
    echo "Running in CI"

    git reset --hard
    git clean -fd
    git checkout "$CI_COMMIT_REF_NAME"
    git pull origin "$CI_COMMIT_REF_NAME"

    case "$CI_COMMIT_REF_NAME" in
        patch-queue/*)
            echo "On a patch queue branch"
            echo "Dropping patches"

            rm -rf debian/patches/
            ;;
        debian/*+git*-*)
            echo "On a packaging tag"

            exec gbp buildpackage -us -uc -b
            ;;
    esac

    echo "Generating a snapshot release"

    gbp dch --snapshot --ignore-branch -c
fi

gbp buildpackage --git-export=WC --git-upstream-tree="$CI_COMMIT_REF_NAME" -us -uc -b
