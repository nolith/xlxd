#!/bin/sh

env_file=upload.env
PKG_VERSION=$(echo $CI_COMMIT_TAG | cut -d '/' -f 2)
if [ "$PKG_VERSION" = "" ]; then
    PKG_VERSION=$(ls debian/build/xlxd_*_amd64.deb | cut -d '_' -f 2)
fi
PKG_AMD64_DEB=xlxd_${PKG_VERSION}_amd64.deb
PKG_AMD64_DEB_URL="${PACKAGE_REGISTRY_URL}${PKG_VERSION}/${PKG_AMD64_DEB}"

echo "PKG_VERSION=${PKG_VERSION}" > "$env_file"
echo "PKG_AMD64_DEB=${PKG_AMD64_DEB}" >> "$env_file"
echo "PKG_AMD64_DEB_URL=${PKG_AMD64_DEB_URL}" >> "$env_file"

echo "Generating $env_file"

cat "$env_file"

echo "Uploading version $PKG_VERSION"

curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
     --upload-file "debian/build/${PKG_AMD64_DEB}" \
     "${PKG_AMD64_DEB_URL}"
