#!/bin/bash

set -e

echo "*********************** Applying PATCHES **************************"

case "$CI_COMMIT_REF_NAME" in
    patch-queue/*) echo "Patches already applied" ;;
    *) QUILT_PATCHES=debian/patches quilt push -a ;;
esac

echo "*********************** PATCHES done **************************"
