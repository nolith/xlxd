FROM debian:10

RUN apt update && apt install build-essential git-buildpackage quilt -y

ENV QUILT_PATCHES=debian/patches
